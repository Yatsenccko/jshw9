const cities = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const itemName = "parent"

function insertArray(array, parent = document.body) {
    let tempUl = document.createElement("ul")
    for (item in array) {
        let tempLi = document.createElement("li")
        tempLi.innerHTML = array[item]
        tempUl.append(tempLi)
    }
    parent.appendChild(tempUl)
}


const elem = document.getElementById(itemName)
insertArray(cities, elem)
